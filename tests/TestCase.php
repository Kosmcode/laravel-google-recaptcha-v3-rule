<?php

namespace Kosmcode\GoogleRecaptchaV3Rule\Tests;

use Illuminate\Foundation\Application;
use Kosmcode\GoogleRecaptchaV3Rule\Providers\GoogleRecaptchaV3RuleServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
    /**
     * Get package providers.
     *
     * @param Application $app
     *
     * @return array
     */
    protected function getPackageProviders($app): array
    {
        return [
            GoogleRecaptchaV3RuleServiceProvider::class,
        ];
    }

    /**
     * Define environment setup.
     *
     * @param Application $app
     *
     * @return void
     */
    protected function getEnvironmentSetUp($app): void
    {
        //
    }
}
