<?php

namespace Kosmcode\GoogleRecaptchaV3Rule\Tests\Unit\app\Services;

use Illuminate\Config\Repository as LaravelConfig;
use Illuminate\Http\Client\Response;
use Illuminate\Log\LogManager as LaravelLog;
use Kosmcode\GoogleRecaptchaV3Rule\Exceptions\GoogleReCaptchaV3Exception;
use Kosmcode\GoogleRecaptchaV3Rule\Repositories\GoogleReCaptchaV3APIRepositoryInterface;
use Kosmcode\GoogleRecaptchaV3Rule\Services\GoogleReCaptchaV3VerifyService;
use Mockery\MockInterface;
use Tests\TestCase;

class GoogleReCaptchaV3VerifyServiceTest extends TestCase
{
    private const EXAMPLE_TOKEN_VALUE = 'captchaToken';
    private const EXAMPLE_CONFIG_KEY_ACCEPTABLE_SCORE = 'google-recaptcha-v3.acceptableScore';
    private const EXAMPLE_CONFIG_ACCEPTABLE_SCORE_VALUE = 0.5;

    public function testVerifyTokenWhenIsNotSetConfigAcceptableScore()
    {
        $mockedGoogleReCaptchaV3APIRepository = $this->mock(GoogleReCaptchaV3APIRepositoryInterface::class);

        $mockedLaravelConfig = $this->mock(
            LaravelConfig::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('get')
                    ->once()
                    ->withArgs([self::EXAMPLE_CONFIG_KEY_ACCEPTABLE_SCORE])
                    ->andReturn(null);
            }
        );

        $mockedLaravelLog = $this->mock(LaravelLog::class);

        $googleReCaptchaV3VerifyService = new GoogleReCaptchaV3VerifyService(
            $mockedGoogleReCaptchaV3APIRepository,
            $mockedLaravelConfig,
            $mockedLaravelLog
        );

        $this->expectException(GoogleReCaptchaV3Exception::class);

        $googleReCaptchaV3VerifyService->verifyToken(self::EXAMPLE_TOKEN_VALUE);
    }

    public function testVerifyTokenWhenSetConfigAcceptableScoreIsEmpty()
    {
        $mockedGoogleReCaptchaV3APIRepository = $this->mock(GoogleReCaptchaV3APIRepositoryInterface::class);

        $mockedLaravelConfig = $this->mock(
            LaravelConfig::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('get')
                    ->once()
                    ->withArgs([self::EXAMPLE_CONFIG_KEY_ACCEPTABLE_SCORE])
                    ->andReturn('');
            }
        );

        $mockedLaravelLog = $this->mock(LaravelLog::class);

        $googleReCaptchaV3VerifyService = new GoogleReCaptchaV3VerifyService(
            $mockedGoogleReCaptchaV3APIRepository,
            $mockedLaravelConfig,
            $mockedLaravelLog
        );

        $this->expectException(GoogleReCaptchaV3Exception::class);

        $googleReCaptchaV3VerifyService->verifyToken(self::EXAMPLE_TOKEN_VALUE);
    }

    public function testVerifyTokenWhenResponseIsError()
    {
        $mockedResponse = $this->mock(
            Response::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('successful')
                    ->once()
                    ->andReturn(false);
                $mock->shouldReceive('body')
                    ->once()
                    ->andReturn('{responseErrorBody}');
            }
        );

        $mockedGoogleReCaptchaV3APIRepository = $this->mock(
            GoogleReCaptchaV3APIRepositoryInterface::class,
            function (MockInterface $mock) use ($mockedResponse) {
                $mock->shouldReceive('verify')
                    ->once()
                    ->withArgs([self::EXAMPLE_TOKEN_VALUE, null])
                    ->andReturn($mockedResponse);
            }
        );

        $mockedLaravelConfig = $this->mock(
            LaravelConfig::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('get')
                    ->once()
                    ->withArgs([self::EXAMPLE_CONFIG_KEY_ACCEPTABLE_SCORE])
                    ->andReturn(self::EXAMPLE_CONFIG_ACCEPTABLE_SCORE_VALUE);
            }
        );

        $mockedLaravelLog = $this->mock(
            LaravelLog::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('error')
                    ->once()
                    ->withAnyArgs()
                    ->andReturnNull();
            }
        );

        $googleReCaptchaV3VerifyService = new GoogleReCaptchaV3VerifyService(
            $mockedGoogleReCaptchaV3APIRepository,
            $mockedLaravelConfig,
            $mockedLaravelLog
        );

        $this->assertFalse($googleReCaptchaV3VerifyService->verifyToken(self::EXAMPLE_TOKEN_VALUE));
    }

    public function testVerifyTokenWhenResponseHasNotSetScore()
    {
        $mockedResponse = $this->mock(
            Response::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('successful')
                    ->once()
                    ->andReturn(true);
                $mock->shouldReceive('json')
                    ->once()
                    ->andReturn([]);
                $mock->shouldReceive('body')
                    ->once()
                    ->andReturn('{responseErrorBody}');
            }
        );

        $mockedGoogleReCaptchaV3APIRepository = $this->mock(
            GoogleReCaptchaV3APIRepositoryInterface::class,
            function (MockInterface $mock) use ($mockedResponse) {
                $mock->shouldReceive('verify')
                    ->once()
                    ->withArgs([self::EXAMPLE_TOKEN_VALUE, null])
                    ->andReturn($mockedResponse);
            }
        );

        $mockedLaravelConfig = $this->mock(
            LaravelConfig::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('get')
                    ->once()
                    ->withArgs([self::EXAMPLE_CONFIG_KEY_ACCEPTABLE_SCORE])
                    ->andReturn(self::EXAMPLE_CONFIG_ACCEPTABLE_SCORE_VALUE);
            }
        );

        $mockedLaravelLog = $this->mock(
            LaravelLog::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('error')
                    ->once()
                    ->withAnyArgs()
                    ->andReturnNull();
            }
        );

        $googleReCaptchaV3VerifyService = new GoogleReCaptchaV3VerifyService(
            $mockedGoogleReCaptchaV3APIRepository,
            $mockedLaravelConfig,
            $mockedLaravelLog
        );

        $this->assertFalse($googleReCaptchaV3VerifyService->verifyToken(self::EXAMPLE_TOKEN_VALUE));
    }

    public function testVerifyTokenWhenResponseSuccessAndScoreIsLessThanExpected()
    {
        $mockedResponse = $this->mock(
            Response::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('successful')
                    ->once()
                    ->andReturn(true);
                $mock->shouldReceive('json')
                    ->twice()
                    ->andReturn(['score' => 0.1]);
                $mock->shouldReceive('body')
                    ->once()
                    ->andReturn('{responseBody}');
            }
        );

        $mockedGoogleReCaptchaV3APIRepository = $this->mock(
            GoogleReCaptchaV3APIRepositoryInterface::class,
            function (MockInterface $mock) use ($mockedResponse) {
                $mock->shouldReceive('verify')
                    ->once()
                    ->withArgs([self::EXAMPLE_TOKEN_VALUE, null])
                    ->andReturn($mockedResponse);
            }
        );

        $mockedLaravelConfig = $this->mock(
            LaravelConfig::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('get')
                    ->once()
                    ->withArgs([self::EXAMPLE_CONFIG_KEY_ACCEPTABLE_SCORE])
                    ->andReturn(self::EXAMPLE_CONFIG_ACCEPTABLE_SCORE_VALUE);
            }
        );

        $mockedLaravelLog = $this->mock(
            LaravelLog::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('info')
                    ->once()
                    ->withAnyArgs()
                    ->andReturnNull();
            }
        );

        $googleReCaptchaV3VerifyService = new GoogleReCaptchaV3VerifyService(
            $mockedGoogleReCaptchaV3APIRepository,
            $mockedLaravelConfig,
            $mockedLaravelLog
        );

        $this->assertFalse($googleReCaptchaV3VerifyService->verifyToken(self::EXAMPLE_TOKEN_VALUE));
    }

    public function testVerifyTokenWhenResponseSuccessAndScoreIsExpected()
    {
        $mockedResponse = $this->mock(
            Response::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('successful')
                    ->once()
                    ->andReturn(true);
                $mock->shouldReceive('json')
                    ->twice()
                    ->andReturn(['score' => 0.8]);
            }
        );

        $mockedGoogleReCaptchaV3APIRepository = $this->mock(
            GoogleReCaptchaV3APIRepositoryInterface::class,
            function (MockInterface $mock) use ($mockedResponse) {
                $mock->shouldReceive('verify')
                    ->once()
                    ->withArgs([self::EXAMPLE_TOKEN_VALUE, null])
                    ->andReturn($mockedResponse);
            }
        );

        $mockedLaravelConfig = $this->mock(
            LaravelConfig::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('get')
                    ->once()
                    ->withArgs([self::EXAMPLE_CONFIG_KEY_ACCEPTABLE_SCORE])
                    ->andReturn(self::EXAMPLE_CONFIG_ACCEPTABLE_SCORE_VALUE);
            }
        );

        $mockedLaravelLog = $this->mock(LaravelLog::class);

        $googleReCaptchaV3VerifyService = new GoogleReCaptchaV3VerifyService(
            $mockedGoogleReCaptchaV3APIRepository,
            $mockedLaravelConfig,
            $mockedLaravelLog
        );

        $this->assertTrue($googleReCaptchaV3VerifyService->verifyToken(self::EXAMPLE_TOKEN_VALUE));
    }
}
