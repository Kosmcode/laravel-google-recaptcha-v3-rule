<?php

namespace Kosmcode\GoogleRecaptchaV3Rule\Tests\Unit\app\Rules;

use Kosmcode\GoogleRecaptchaV3Rule\Rules\GoogleReCaptchaV3CheckRule;
use Kosmcode\GoogleRecaptchaV3Rule\Services\GoogleReCaptchaV3VerifyServiceInterface;
use Mockery\MockInterface;
use Tests\TestCase;

class GoogleReCaptchaV3CheckRuleTest extends TestCase
{
    private const EXAMPLE_ATTRIBUTE = 'captcha';
    private const EXAMPLE_VALUE = 'captchaToken';
    private const EXAMPLE_IP_ADDRESS = '123.123.123.123';

    public function testWhenNotSetClientIpAndNotPasses()
    {
        $this->mock(
            GoogleReCaptchaV3VerifyServiceInterface::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('verifyToken')
                    ->once()
                    ->withArgs([self::EXAMPLE_VALUE, null])
                    ->andReturn(false);
            }
        );

        $googleReCaptchaV3CheckRule = new GoogleReCaptchaV3CheckRule();

        $this->assertFalse(
            $googleReCaptchaV3CheckRule->passes(self::EXAMPLE_ATTRIBUTE, self::EXAMPLE_VALUE)
        );
        $this->assertStringContainsString(
            $googleReCaptchaV3CheckRule->message(),
            'Google ReCaptcha V3 token is invalid.'
        );
    }

    public function testWhenNotSetClientIpAndPasses()
    {
        $this->mock(
            GoogleReCaptchaV3VerifyServiceInterface::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('verifyToken')
                    ->once()
                    ->withArgs([self::EXAMPLE_VALUE, null])
                    ->andReturn(true);
            }
        );

        $googleReCaptchaV3CheckRule = new GoogleReCaptchaV3CheckRule();

        $this->assertTrue(
            $googleReCaptchaV3CheckRule->passes(self::EXAMPLE_ATTRIBUTE, self::EXAMPLE_VALUE)
        );
    }

    public function testWhenIsSetClientIpAndNotPasses()
    {
        $this->mock(
            GoogleReCaptchaV3VerifyServiceInterface::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('verifyToken')
                    ->once()
                    ->withArgs([self::EXAMPLE_VALUE, self::EXAMPLE_IP_ADDRESS])
                    ->andReturn(false);
            }
        );

        $googleReCaptchaV3CheckRule = new GoogleReCaptchaV3CheckRule(self::EXAMPLE_IP_ADDRESS);

        $this->assertFalse(
            $googleReCaptchaV3CheckRule->passes(self::EXAMPLE_ATTRIBUTE, self::EXAMPLE_VALUE)
        );
        $this->assertStringContainsString(
            $googleReCaptchaV3CheckRule->message(),
            'Google ReCaptcha V3 token is invalid.'
        );
    }

    public function testWhenIsSetClientIpAndPasses()
    {
        $this->mock(
            GoogleReCaptchaV3VerifyServiceInterface::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('verifyToken')
                    ->once()
                    ->withArgs([self::EXAMPLE_VALUE, self::EXAMPLE_IP_ADDRESS])
                    ->andReturn(true);
            }
        );

        $googleReCaptchaV3CheckRule = new GoogleReCaptchaV3CheckRule(self::EXAMPLE_IP_ADDRESS);

        $this->assertTrue(
            $googleReCaptchaV3CheckRule->passes(self::EXAMPLE_ATTRIBUTE, self::EXAMPLE_VALUE)
        );
    }
}
