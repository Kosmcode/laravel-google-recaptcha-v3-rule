<?php

namespace Kosmcode\GoogleRecaptchaV3Rule\Tests\Unit\app\Repositories;

use Illuminate\Config\Repository as LaravelConfig;
use Illuminate\Http\Client\Factory as HTTPClient;
use Illuminate\Http\Client\Response;
use Kosmcode\GoogleRecaptchaV3Rule\Exceptions\GoogleReCaptchaV3Exception;
use Kosmcode\GoogleRecaptchaV3Rule\Repositories\GoogleReCaptchaV3APIRepository;
use Mockery\MockInterface;
use Tests\TestCase;

class GoogleReCaptchaV3APIRepositoryTest extends TestCase
{
    private const EXAMPLE_CONFIG_KEY_SECRET_KEY = 'google-recaptcha-v3.secretKey';
    private const EXAMPLE_TOKEN_VALUE = 'captchaToken';
    private const EXAMPLE_SECRET_KEY = 'secretKey';

    public function testVerifyWhenIsNotSetSecretKeyInConfig()
    {
        $mockedHTTPClient = $this->mock(HTTPClient::class);

        $mockedLaravelConfig = $this->mock(
            LaravelConfig::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('get')
                    ->once()
                    ->withArgs([self::EXAMPLE_CONFIG_KEY_SECRET_KEY])
                    ->andReturn(null);
            }
        );

        $mockedGoogleReCaptchaV3APIRepository = new GoogleReCaptchaV3APIRepository(
            $mockedHTTPClient,
            $mockedLaravelConfig
        );

        $this->expectException(GoogleReCaptchaV3Exception::class);

        $this->assertFalse(
            $mockedGoogleReCaptchaV3APIRepository->verify(self::EXAMPLE_TOKEN_VALUE)
        );
    }

    public function testVerifyWhenSetSecretKeyInConfigIsEmpty()
    {
        $mockedHTTPClient = $this->mock(HTTPClient::class);

        $mockedLaravelConfig = $this->mock(
            LaravelConfig::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('get')
                    ->once()
                    ->withArgs([self::EXAMPLE_CONFIG_KEY_SECRET_KEY])
                    ->andReturn('');
            }
        );

        $mockedGoogleReCaptchaV3APIRepository = new GoogleReCaptchaV3APIRepository(
            $mockedHTTPClient,
            $mockedLaravelConfig
        );

        $this->expectException(GoogleReCaptchaV3Exception::class);

        $this->assertInstanceOf(
            Response::class,
            $mockedGoogleReCaptchaV3APIRepository->verify(self::EXAMPLE_TOKEN_VALUE)
        );
    }

    public function testVerifyWhenReturnResponse()
    {
        $mockedResponse = $this->mock(Response::class);

        $mockedHTTPClient = $this->mock(
            HTTPClient::class,
            function (MockInterface $mock) use ($mockedResponse) {
                $mock->shouldReceive('withHeaders')
                    ->once()
                    ->withAnyArgs()
                    ->andReturnSelf();
                $mock->shouldReceive('asForm')
                    ->once()
                    ->andReturnSelf();
                $mock->shouldReceive('post')
                    ->once()
                    ->withAnyArgs()
                    ->andReturn($mockedResponse);
            }
        );

        $mockedLaravelConfig = $this->mock(
            LaravelConfig::class,
            function (MockInterface $mock) {
                $mock->shouldReceive('get')
                    ->once()
                    ->withArgs([self::EXAMPLE_CONFIG_KEY_SECRET_KEY])
                    ->andReturn(self::EXAMPLE_SECRET_KEY);
            }
        );

        $mockedGoogleReCaptchaV3APIRepository = new GoogleReCaptchaV3APIRepository(
            $mockedHTTPClient,
            $mockedLaravelConfig
        );

        $this->assertInstanceOf(
            Response::class,
            $mockedGoogleReCaptchaV3APIRepository->verify(self::EXAMPLE_TOKEN_VALUE)
        );
    }
}
