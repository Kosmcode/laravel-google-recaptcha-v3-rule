<?php

return [
    'secretKey' => env('GOOGLE_RECAPTCHA_V3_SECRET_KEY'),
    'acceptableScore' => env('GOOGLE_RECAPTCHA_V3_ACCEPTABLE_SCORE', 0.5), // float value
];
