# google-recaptcha-v3-rule

[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)
[![Travis](https://img.shields.io/travis/kosmcode/google-recaptcha-v3-rule.svg?style=flat-square)]()
[![Total Downloads](https://img.shields.io/packagist/dt/kosmcode/google-recaptcha-v3-rule.svg?style=flat-square)](https://packagist.org/packages/kosmcode/google-recaptcha-v3-rule)

## Install

```bash
composer require kosmcode/google-recaptcha-v3-rule
````

```bash
php artisan vendor:publish --provider="Kosmcode\GoogleRecaptchaV3Rule\Providers\GoogleRecaptchaV3RuleServiceProvider" --tag="config"
```

Add to env (or if you prefer set values in published config)

```dotenv
GOOGLE_RECAPTCHA_V3_SECRET_KEY= # Secret key
GOOGLE_RECAPTCHA_V3_ACCEPTABLE_SCORE= # Optionaly, default 0.5, must be float
```

## Usage

### Rule

Use in form request or where you want

```php
use Kosmcode\GoogleRecaptchaV3Rule\Rules\GoogleReCaptchaV3CheckRule;
...
class ExampleRequest extends AbstractFormRequest
...
    public function rules(): array
    {
        return [
            ...
            'captcha' => [
                'required',
                new GoogleReCaptchaV3CheckRule($this->getClientIp()),
            ],
        ];
    }
```

### Facade

(Optional) Add to `app.php` in `config/` dir to `aliases`:

```php
'aliases' => [
    ...
    'View' => Illuminate\Support\Facades\View::class,
    'GoogleReCaptchaV3Facade' => Kosmcode\GoogleRecaptchaV3Rule\Facades\GoogleReCaptchaV3Facade::class,
],
```

```php
use GoogleReCaptchaV3Facade;
or
use Kosmcode\GoogleRecaptchaV3Rule\Facades\GoogleReCaptchaV3Facade;

GoogleReCaptchaV3Facade::verifyToken($captchaToken, $optionalClientIpAddress);
```

### Service

```php
use Kosmcode\GoogleRecaptchaV3Rule\Services\GoogleReCaptchaV3VerifyServiceInterface;

class MyClass {

    protected GoogleReCaptchaV3VerifyServiceInterface $service;

    public function __construct(GoogleReCaptchaV3VerifyServiceInterface $service) {
        $this->service = $service;
    }
    
    public function myMethod() {
        $result = $this->service->verifyToken($tokenCaptcha, $optionalClientIpAddress);
    }

}
```

### API Repository

```php
use Kosmcode\GoogleRecaptchaV3Rule\Repositories\GoogleReCaptchaV3APIRepositoryInterface;

class MyClass {

    protected GoogleReCaptchaV3APIRepositoryInterface $repository;

    public function __construct(GoogleReCaptchaV3APIRepositoryInterface $repository) {
        $this->repository = $repository;
    }
    
    public function myMethod() {
        $response = $this->repository->verify($tokenCaptcha, $optionalClientIpAddress);
    }

}
```

## Testing

Run the tests with:

``` bash
vendor/bin/phpunit
```

## Credits

- [KosmCODE](https://gitlab.com/Kosmcode)

## Security

If you discover any security-related issues, please email kontakt@kosmcode.pl instead of using the issue tracker.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
