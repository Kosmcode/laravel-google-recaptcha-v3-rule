<?php

namespace Kosmcode\GoogleRecaptchaV3Rule\Exceptions;

use Exception;

class GoogleReCaptchaV3Exception extends Exception
{
}
