<?php

namespace Kosmcode\GoogleRecaptchaV3Rule\Services;

use Illuminate\Config\Repository as LaravelConfig;
use Illuminate\Log\LogManager as LaravelLog;
use Kosmcode\GoogleRecaptchaV3Rule\Exceptions\GoogleReCaptchaV3Exception;
use Kosmcode\GoogleRecaptchaV3Rule\Repositories\GoogleReCaptchaV3APIRepositoryInterface;

/**
 * Class of Google ReCaptcha V3 Verify Service
 */
class GoogleReCaptchaV3VerifyService implements GoogleReCaptchaV3VerifyServiceInterface
{
    private GoogleReCaptchaV3APIRepositoryInterface $googleReCaptchaV3APIRepository;
    private LaravelLog $laravelLog;
    private LaravelConfig $laravelConfig;


    public function __construct(
        GoogleReCaptchaV3APIRepositoryInterface $googleReCaptchaV3APIRepository,
        LaravelConfig                           $laravelConfig,
        LaravelLog                              $laravelLog
    )
    {
        $this->googleReCaptchaV3APIRepository = $googleReCaptchaV3APIRepository;
        $this->laravelLog = $laravelLog;
        $this->laravelConfig = $laravelConfig;
    }

    /**
     * {@inheritDoc}
     */
    public function verifyToken(string $token, ?string $clientIP = null): bool
    {
        $googleReCaptchaV3AcceptableScore = $this->laravelConfig->get('google-recaptcha-v3.acceptableScore');

        if (empty($googleReCaptchaV3AcceptableScore)) {
            throw new GoogleReCaptchaV3Exception('Acceptable score is not set in config');
        }

        $response = $this->googleReCaptchaV3APIRepository->verify($token, $clientIP);

        if ($response->successful() === false || !isset($response->json()['score'])) {
            $this->laravelLog->error(
                'Google ReCaptcha V3 : Response error',
                [
                    'responseBody' => $response->body(),
                    'token' => $token,
                    'clientIP' => $clientIP,
                ]
            );

            return false;
        }

        if ((float)$response->json()['score'] < $googleReCaptchaV3AcceptableScore) {
            $this->laravelLog->info(
                'Google ReCaptcha V3 : Less than acceptable score',
                [
                    'responseBody' => $response->body(),
                    'token' => $token,
                    'clientIP' => $clientIP,
                ]
            );

            return false;
        }

        return true;
    }
}
