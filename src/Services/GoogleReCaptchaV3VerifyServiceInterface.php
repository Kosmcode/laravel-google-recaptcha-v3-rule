<?php

namespace Kosmcode\GoogleRecaptchaV3Rule\Services;

use Kosmcode\GoogleRecaptchaV3Rule\Exceptions\GoogleReCaptchaV3Exception;

/**
 * Interface of Google ReCaptcha V3 Verify Service
 */
interface GoogleReCaptchaV3VerifyServiceInterface
{
    /**
     * Method to verify Google ReCapcha v3 token
     *
     * @param string $token
     * @param string|null $clientIP
     *
     * @return bool
     *
     * @throws GoogleReCaptchaV3Exception
     */
    public function verifyToken(string $token, ?string $clientIP = null): bool;
}
