<?php

namespace Kosmcode\GoogleRecaptchaV3Rule\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\App;
use Kosmcode\GoogleRecaptchaV3Rule\Exceptions\GoogleReCaptchaV3Exception;
use Kosmcode\GoogleRecaptchaV3Rule\Services\GoogleReCaptchaV3VerifyServiceInterface;

/**
 * Class of Google ReCaptcha V3 Check Rule
 */
class GoogleReCaptchaV3CheckRule implements Rule
{
    private GoogleReCaptchaV3VerifyServiceInterface $googleReCaptchaV3VerifyService;
    private ?string $clientIP;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(
        ?string $clientIP = null
    )
    {
        $this->clientIP = $clientIP;
        $this->googleReCaptchaV3VerifyService = App::make(GoogleReCaptchaV3VerifyServiceInterface::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     *
     * @return bool
     *
     * @throws GoogleReCaptchaV3Exception
     */
    public function passes($attribute, $value): bool
    {
        return $this->googleReCaptchaV3VerifyService->verifyToken($value, $this->clientIP);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Google ReCaptcha V3 token is invalid.';
    }
}
