<?php

namespace Kosmcode\GoogleRecaptchaV3Rule\Providers;

use Illuminate\Config\Repository as LaravelConfig;
use Illuminate\Log\LogManager as LaravelLog;
use Illuminate\Support\ServiceProvider;
use Kosmcode\GoogleRecaptchaV3Rule\Repositories\GoogleReCaptchaV3APIRepository;
use Kosmcode\GoogleRecaptchaV3Rule\Repositories\GoogleReCaptchaV3APIRepositoryInterface;
use Kosmcode\GoogleRecaptchaV3Rule\Services\GoogleReCaptchaV3VerifyService;
use Kosmcode\GoogleRecaptchaV3Rule\Services\GoogleReCaptchaV3VerifyServiceInterface;

class GoogleRecaptchaV3RuleServiceProvider extends ServiceProvider
{
    /**
     * @var array|string[]
     */
    public array $bindings = [
        GoogleReCaptchaV3APIRepositoryInterface::class => GoogleReCaptchaV3APIRepository::class,
        GoogleReCaptchaV3VerifyServiceInterface::class => GoogleReCaptchaV3VerifyService::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind('GoogleReCaptchaV3', function ($app) {
            return new GoogleReCaptchaV3VerifyService(
                $app->make(GoogleReCaptchaV3APIRepositoryInterface::class),
                $app->make(LaravelConfig::class),
                $app->make(LaravelLog::class)
            );
        });

        $this->mergeConfigFrom(
            __DIR__ . '/../../config/config.php',
            'google-recaptcha-v3'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {

            $this->publishes(
                [
                    __DIR__ . '/../../config/config.php' => config_path('google-recaptcha-v3.php'),
                ],
                'config'
            );

        }
    }
}
