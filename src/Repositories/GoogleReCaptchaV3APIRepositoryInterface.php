<?php

namespace Kosmcode\GoogleRecaptchaV3Rule\Repositories;

use Illuminate\Http\Client\Response;
use Kosmcode\GoogleRecaptchaV3Rule\Exceptions\GoogleReCaptchaV3Exception;

/**
 * Interface of Google ReCaptcha V3 API Repository
 */
interface GoogleReCaptchaV3APIRepositoryInterface
{
    const GOOGLE_RECAPTCHA_API_URL = 'https://www.google.com/recaptcha/api/';
    const GOOGLE_RECAPTCHA_ENDPOINT_VERIFY = 'siteverify';

    /**
     * Method to verify Google ReCaptcha V3 token
     * Docs: https://developers.google.com/recaptcha/docs/verify
     *
     * @param string $token
     * @param string|null $clientIP
     *
     * @return Response
     *
     * @throws GoogleReCaptchaV3Exception
     */
    public function verify(string $token, ?string $clientIP = null): Response;
}
