<?php

namespace Kosmcode\GoogleRecaptchaV3Rule\Repositories;

use Illuminate\Config\Repository as LaravelConfig;
use Illuminate\Http\Client\Factory as HTTPClient;
use Illuminate\Http\Client\Response;
use Kosmcode\GoogleRecaptchaV3Rule\Exceptions\GoogleReCaptchaV3Exception;

/**
 * Class of Google ReCaptcha V3 API Repository
 */
class GoogleReCaptchaV3APIRepository implements GoogleReCaptchaV3APIRepositoryInterface
{
    private HTTPClient $httpClient;
    private LaravelConfig $laravelConfig;

    public function __construct(
        HTTPClient    $httpClient,
        LaravelConfig $laravelConfig
    )
    {
        $this->httpClient = $httpClient;
        $this->laravelConfig = $laravelConfig;
    }

    /**
     * {@inheritDoc}
     */
    public function verify(string $token, ?string $clientIP = null): Response
    {
        $googleReCaptchaV3SecretKey = $this->laravelConfig->get('google-recaptcha-v3.secretKey');

        if (empty($googleReCaptchaV3SecretKey)) {
            throw new GoogleReCaptchaV3Exception('Secret key is not set config');
        }

        $data = [
            'secret' => $googleReCaptchaV3SecretKey,
            'response' => $token,
        ];

        if ($clientIP !== null) {
            $data['remoteip'] = $clientIP;
        }

        return $this->httpClient
            ->withHeaders(['Content-Type' => 'application/x-www-form-urlencoded'])
            ->asForm()
            ->post(
                self::GOOGLE_RECAPTCHA_API_URL . self::GOOGLE_RECAPTCHA_ENDPOINT_VERIFY,
                $data
            );
    }
}
