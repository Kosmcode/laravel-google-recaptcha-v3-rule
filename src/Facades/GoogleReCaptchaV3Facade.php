<?php

namespace Kosmcode\GoogleRecaptchaV3Rule\Facades;

use Illuminate\Support\Facades\Facade;

class GoogleReCaptchaV3Facade extends Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor(): string
    {
        return 'GoogleReCaptchaV3';
    }
}
